# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.
use Mix.Config

# General application configuration
config :bachelor_thesis,
  ecto_repos: [BachelorThesis.Repo]

# Configures the endpoint
config :bachelor_thesis, BachelorThesis.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "jOeEeHXfhhsIm9HjJJBTQoOS9eq+IGlpkGSoORfiuULNvAoCTqpLVGLVCtT789B9",
  render_errors: [view: BachelorThesis.ErrorView, accepts: ~w(html json)],
  pubsub: [name: BachelorThesis.PubSub,
           adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env}.exs"
