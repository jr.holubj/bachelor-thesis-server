defmodule BachelorThesis.SmartDeviceControllerTest do
  use BachelorThesis.ConnCase

  alias BachelorThesis.SmartDevice
  @valid_attrs %{active: true, description: "some content", name: "some content", pairedUp: true, token: "some content"}
  @invalid_attrs %{}

  test "lists all entries on index", %{conn: conn} do
    conn = get conn, smart_device_path(conn, :index)
    assert html_response(conn, 200) =~ "Listing smart devices"
  end

  test "renders form for new resources", %{conn: conn} do
    conn = get conn, smart_device_path(conn, :new)
    assert html_response(conn, 200) =~ "New smart device"
  end

  test "creates resource and redirects when data is valid", %{conn: conn} do
    conn = post conn, smart_device_path(conn, :create), smart_device: @valid_attrs
    assert redirected_to(conn) == smart_device_path(conn, :index)
    assert Repo.get_by(SmartDevice, @valid_attrs)
  end

  test "does not create resource and renders errors when data is invalid", %{conn: conn} do
    conn = post conn, smart_device_path(conn, :create), smart_device: @invalid_attrs
    assert html_response(conn, 200) =~ "New smart device"
  end

  test "shows chosen resource", %{conn: conn} do
    smart_device = Repo.insert! %SmartDevice{}
    conn = get conn, smart_device_path(conn, :show, smart_device)
    assert html_response(conn, 200) =~ "Show smart device"
  end

  test "renders page not found when id is nonexistent", %{conn: conn} do
    assert_error_sent 404, fn ->
      get conn, smart_device_path(conn, :show, -1)
    end
  end

  test "renders form for editing chosen resource", %{conn: conn} do
    smart_device = Repo.insert! %SmartDevice{}
    conn = get conn, smart_device_path(conn, :edit, smart_device)
    assert html_response(conn, 200) =~ "Edit smart device"
  end

  test "updates chosen resource and redirects when data is valid", %{conn: conn} do
    smart_device = Repo.insert! %SmartDevice{}
    conn = put conn, smart_device_path(conn, :update, smart_device), smart_device: @valid_attrs
    assert redirected_to(conn) == smart_device_path(conn, :show, smart_device)
    assert Repo.get_by(SmartDevice, @valid_attrs)
  end

  test "does not update chosen resource and renders errors when data is invalid", %{conn: conn} do
    smart_device = Repo.insert! %SmartDevice{}
    conn = put conn, smart_device_path(conn, :update, smart_device), smart_device: @invalid_attrs
    assert html_response(conn, 200) =~ "Edit smart device"
  end

  test "deletes chosen resource", %{conn: conn} do
    smart_device = Repo.insert! %SmartDevice{}
    conn = delete conn, smart_device_path(conn, :delete, smart_device)
    assert redirected_to(conn) == smart_device_path(conn, :index)
    refute Repo.get(SmartDevice, smart_device.id)
  end
end
