defmodule BachelorThesis.DeviceTest do
  use BachelorThesis.ModelCase

  alias BachelorThesis.Device

  @valid_attrs %{active: true, description: "some content", name: "some content", pairedUp: true, token: "some content"}
  @invalid_attrs %{}

  test "changeset with valid attributes" do
    changeset = Device.changeset(%Device{}, @valid_attrs)
    assert changeset.valid?
  end

  test "changeset with invalid attributes" do
    changeset = Device.changeset(%Device{}, @invalid_attrs)
    refute changeset.valid?
  end
end
