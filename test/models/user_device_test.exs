defmodule BachelorThesis.UserDeviceTest do
  use BachelorThesis.ModelCase

  alias BachelorThesis.UserDevice

  @valid_attrs %{}
  @invalid_attrs %{}

  test "changeset with valid attributes" do
    changeset = UserDevice.changeset(%UserDevice{}, @valid_attrs)
    assert changeset.valid?
  end

  test "changeset with invalid attributes" do
    changeset = UserDevice.changeset(%UserDevice{}, @invalid_attrs)
    refute changeset.valid?
  end
end
