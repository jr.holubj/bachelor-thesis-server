defmodule BachelorThesis.PairDeviceTest do
  use BachelorThesis.ModelCase

  alias BachelorThesis.PairDevice

  @valid_attrs %{deadline: %{day: 17, hour: 14, min: 0, month: 4, sec: 0, year: 2010}, successful: true, token: "some content"}
  @invalid_attrs %{}

  test "changeset with valid attributes" do
    changeset = PairDevice.changeset(%PairDevice{}, @valid_attrs)
    assert changeset.valid?
  end

  test "changeset with invalid attributes" do
    changeset = PairDevice.changeset(%PairDevice{}, @invalid_attrs)
    refute changeset.valid?
  end
end
