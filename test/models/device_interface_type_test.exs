defmodule BachelorThesis.DeviceInterfaceTypeTest do
  use BachelorThesis.ModelCase

  alias BachelorThesis.DeviceInterfaceType

  @valid_attrs %{name: "some content", shortcut: "some content"}
  @invalid_attrs %{}

  test "changeset with valid attributes" do
    changeset = DeviceInterfaceType.changeset(%DeviceInterfaceType{}, @valid_attrs)
    assert changeset.valid?
  end

  test "changeset with invalid attributes" do
    changeset = DeviceInterfaceType.changeset(%DeviceInterfaceType{}, @invalid_attrs)
    refute changeset.valid?
  end
end
