defmodule BachelorThesis.DeviceActionTest do
  use BachelorThesis.ModelCase

  alias BachelorThesis.DeviceAction

  @valid_attrs %{boolValue: true, intValue: 42}
  @invalid_attrs %{}

  test "changeset with valid attributes" do
    changeset = DeviceAction.changeset(%DeviceAction{}, @valid_attrs)
    assert changeset.valid?
  end

  test "changeset with invalid attributes" do
    changeset = DeviceAction.changeset(%DeviceAction{}, @invalid_attrs)
    refute changeset.valid?
  end
end
