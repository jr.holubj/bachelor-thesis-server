defmodule BachelorThesis.DeviceValueTest do
  use BachelorThesis.ModelCase

  alias BachelorThesis.DeviceValue

  @valid_attrs %{boolValue: true, intValue: 42}
  @invalid_attrs %{}

  test "changeset with valid attributes" do
    changeset = DeviceValue.changeset(%DeviceValue{}, @valid_attrs)
    assert changeset.valid?
  end

  test "changeset with invalid attributes" do
    changeset = DeviceValue.changeset(%DeviceValue{}, @invalid_attrs)
    refute changeset.valid?
  end
end
