# Script for populating the database. You can run it as:
#
#     mix run priv/repo/seeds.exs
#
# Inside the script, you can read and write to any of your
# repositories directly:
#
#     BachelorThesis.Repo.insert!(%BachelorThesis.SomeModel{})
#
# We recommend using the bang functions (`insert!`, `update!`
# and so on) as they will fail if something goes wrong.
alias BachelorThesis.Repo
alias BachelorThesis.DeviceInterfaceType

Repo.insert!(%DeviceInterfaceType{
  name: "Yes/No",
  shortcut: "yn"
})
Repo.insert!(%DeviceInterfaceType{
  name: "Number",
  shortcut: "num"
})
Repo.insert!(%DeviceInterfaceType{
  name: "Start",
  shortcut: "st"
})
