defmodule BachelorThesis.Repo.Migrations.CreatePairSmartDevice do
  use Ecto.Migration

  def change do
    create table(:pair_smart_device) do
      add :token, :string
      add :successful, :boolean, default: false, null: false
      add :deadline, :utc_datetime
      add :smart_device_id, references(:smart_devices, on_delete: :nothing)

      timestamps()
    end
    create index(:pair_smart_device, [:smart_device_id])

  end
end
