defmodule BachelorThesis.Repo.Migrations.CreateDevice do
  use Ecto.Migration

  def change do
    create table(:devices) do
      add :token, :string
      add :name, :string
      add :description, :string
      add :pairedUp, :boolean, default: false, null: false
      add :active, :boolean, default: false, null: false

      timestamps()
    end
    create unique_index(:devices, [:token])

  end
end
