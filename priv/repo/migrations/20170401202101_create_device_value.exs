defmodule BachelorThesis.Repo.Migrations.CreateDeviceValue do
  use Ecto.Migration

  def change do
    create table(:device_values) do
      add :intValue, :integer, default: nil, null: true
      add :boolValue, :boolean, default: nil, null: true
      add :device_id, references(:devices, on_delete: :nothing)
      add :device_interface_type_id, references(:device_interface_types, on_delete: :nothing)
      add :datetime, :utc_datetime

      timestamps()
    end
    create index(:device_values, [:device_id])
    create index(:device_values, [:device_interface_type_id])

  end
end
