defmodule BachelorThesis.Repo.Migrations.CreateUserDevice do
  use Ecto.Migration

  def change do
    create table(:user_devices) do
      add :user_id, references(:users, on_delete: :nothing)
      add :device_id, references(:devices, on_delete: :nothing)

      timestamps()
    end
    create index(:user_devices, [:user_id])
    create index(:user_devices, [:device_id])

  end
end
