defmodule BachelorThesis.Repo.Migrations.CreatePairDevice do
  use Ecto.Migration

  def change do
    create table(:pair_devices) do
      add :token, :string
      add :successful, :boolean, default: false, null: false
      add :deadline, :datetime
      add :device_id, references(:devices, on_delete: :nothing)

      timestamps()
    end
    create index(:pair_devices, [:device_id])

  end
end
