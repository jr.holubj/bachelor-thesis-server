defmodule BachelorThesis.Repo.Migrations.UpdateDeviceTable do
  use Ecto.Migration

  def change do
    alter table(:devices) do
      add :interface_type_id, :int
    end
  end
end
