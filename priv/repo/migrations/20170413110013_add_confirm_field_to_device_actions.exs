defmodule BachelorThesis.Repo.Migrations.AddConfirmFieldToDeviceActions do
  use Ecto.Migration

  def change do
    alter table(:device_actions) do
      add :confirmed, :boolean, default: false
    end
  end
end
