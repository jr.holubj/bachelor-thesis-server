defmodule BachelorThesis.Repo.Migrations.CreateUser do
  use Ecto.Migration

  def change do
    create table(:users) do
      add :email, :string
      add :password, :string
      add :firstName, :string
      add :lastName, :string
      add :admin, :boolean, default: false, null: false

      timestamps()
    end
    create unique_index(:users, [:email])

  end
end
