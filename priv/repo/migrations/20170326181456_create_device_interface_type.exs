defmodule BachelorThesis.Repo.Migrations.CreateDeviceInterfaceType do
  use Ecto.Migration

  def change do
    create table(:device_interface_types) do
      add :name, :string
      add :shortcut, :string

      timestamps()
    end
    create unique_index(:device_interface_types, [:shortcut])

  end
end
