defmodule BachelorThesis.Repo.Migrations.CreateSmartDevice do
  use Ecto.Migration

  def change do
    create table(:smart_devices) do
      add :token, :string
      add :name, :string
      add :description, :text
      add :pairedUp, :boolean, default: false, null: false
      add :active, :boolean, default: false, null: false
      add :user_id, references(:users, on_delete: :nothing)

      timestamps()
    end
    create unique_index(:smart_devices, [:token])
    create index(:smart_devices, [:user_id])

  end
end
