defmodule BachelorThesis.Plug.Authenticate do
  @moduledoc """
    Authenticate plug module allows access to logged users. Otherwise redirects conn to root page
  """
  import Plug.Conn
  import Phoenix.Controller, only: [put_flash: 3, redirect: 2]

  def init(default) do
    default
  end

  def call(conn, _default) do
    conn |> authenticate
  end

  defp authenticate(conn) do
    case BachelorThesis.Session.logged_in?(conn) do
      false -> conn |> put_flash(:error, "You are not logged in") |> redirect(to: "/") |> halt
      _ -> conn
    end
  end
end
