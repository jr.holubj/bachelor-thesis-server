defmodule BachelorThesis.DeviceInterfaceType do
  use BachelorThesis.Web, :model

  alias BachelorThesis.DeviceInterfaceType
  alias BachelorThesis.Repo

  schema "device_interface_types" do
    field :name, :string
    field :shortcut, :string

    timestamps()
  end

  @doc """
  Builds a changeset based on the `struct` and `params`.
  """
  def changeset(struct, params \\ %{}) do
    struct
    |> cast(params, [:name, :shortcut])
    |> validate_required([:name, :shortcut])
    |> unique_constraint(:shortcut)
  end


  @doc """
  Function returns deviceInterfaceType by selected id
  """
  def get_by_id(id) do
    query = from u in DeviceInterfaceType,
      where: u.id == ^id
    Repo.one(query)
  end

  @doc """
  Function returns deviceInterfaceType by selected shortcut
  """
  def get_by_shortcut(shortcut) do
    query = from u in DeviceInterfaceType,
      where: u.shortcut == ^shortcut
    Repo.one(query)
  end
end
