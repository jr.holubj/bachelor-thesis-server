defmodule BachelorThesis.SmartDevice do
  use BachelorThesis.Web, :model

  alias BachelorThesis.SmartDevice
  alias BachelorThesis.Repo

  schema "smart_devices" do
    field :token, :string
    field :name, :string
    field :description, :string
    field :pairedUp, :boolean, default: false
    field :active, :boolean, default: false
    belongs_to :user, BachelorThesis.User
    has_many :pair_smart_devices, BachelorThesis.PairSmartDevice, on_delete: :delete_all

    timestamps()
  end

  @doc """
  Builds a changeset based on the `struct` and `params`.
  """
  def changeset(struct, params \\ %{}) do
    struct
    |> cast(params, [:token, :name, :description, :pairedUp, :active, :user_id])
    |> validate_required([:token, :name, :description, :pairedUp, :active])
    |> unique_constraint(:token)
  end

  @doc """
  Function returns SmartDevice by selected token
  """
  def get_by_token(token) do
    query = from u in SmartDevice,
      where: u.token == ^token
    Repo.one(query)
  end
end
