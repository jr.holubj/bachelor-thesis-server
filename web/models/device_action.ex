defmodule BachelorThesis.DeviceAction do
  use BachelorThesis.Web, :model

  alias BachelorThesis.DeviceAction
  alias BachelorThesis.Repo

  schema "device_actions" do
    field :intValue, :integer
    field :boolValue, :boolean, default: false
    field :confirmed, :boolean, default: false
    field :datetime, Ecto.DateTime
    belongs_to :device, BachelorThesis.Device
    belongs_to :smart_device, BachelorThesis.SmartDevice
    belongs_to :device_interface_type, BachelorThesis.DeviceInterfaceType

    timestamps()
  end

  @doc """
  Builds a changeset based on the `struct` and `params`.
  """
  def changeset(struct, params \\ %{}) do
    struct
    |> cast(params, [:intValue, :boolValue, :datetime, :device_id, :smart_device_id, :device_interface_type_id, :confirmed])
  end

  @doc """
  Function returns deviceAction by selected id
  """
  def get_by_id(id) do
    query = from u in DeviceAction,
      where: u.id == ^id
    Repo.one(query)
  end

end
