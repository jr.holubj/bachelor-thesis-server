defmodule BachelorThesis.PairSmartDevice do
  use BachelorThesis.Web, :model

  alias BachelorThesis.PairSmartDevice
  alias BachelorThesis.Repo

  schema "pair_smart_device" do
    field :token, :string
    field :successful, :boolean, default: false
    field :deadline, Ecto.DateTime
    belongs_to :smart_device, BachelorThesis.SmartDevice

    timestamps()
  end

  @doc """
  Builds a changeset based on the `struct` and `params`.
  """
  def changeset(struct, params \\ %{}) do
    struct
    |> cast(params, [:token, :successful, :deadline, :smart_device_id])
    |> validate_required([:token, :successful, :deadline])
  end


  @doc """
  Function returns PairSmartDevice by selected token
  """
  def get_by_token(token) do
    now = Ecto.DateTime.utc
    query = from u in PairSmartDevice,
      where: u.token == ^token and u.deadline >= ^now and u.successful == ^false
    Repo.one(query) |> Repo.preload([:smart_device])
  end
end
