defmodule BachelorThesis.PairDevice do
  use BachelorThesis.Web, :model

  alias BachelorThesis.PairDevice
  alias BachelorThesis.Repo

  schema "pair_devices" do
    field :token, :string
    field :successful, :boolean, default: false
    field :deadline, Ecto.DateTime
    belongs_to :device, BachelorThesis.Device

    timestamps()
  end

  @doc """
  Builds a changeset based on the `struct` and `params`.
  """
  def changeset(struct, params \\ %{}) do
    struct
    |> cast(params, [:token, :successful, :deadline, :device_id])
    |> validate_required([:token, :successful, :deadline])
  end

  @doc """
  Function returns PairDevice by selected token
  """
  def get_by_token(token) do
    now = Ecto.DateTime.utc
    query = from u in PairDevice,
      where: u.token == ^token and u.deadline >= ^now and u.successful == ^false
    Repo.one(query) |> Repo.preload([:device])
  end
end
