defmodule BachelorThesis.User do
  use BachelorThesis.Web, :model

  alias BachelorThesis.Repo
  alias BachelorThesis.User

  schema "users" do
    field :email, :string
    field :password, :string
    field :firstName, :string
    field :lastName, :string
    field :admin, :boolean, default: false

    has_many :smart_devices, BachelorThesis.SmartDevice, on_delete: :delete_all
    many_to_many :devices, BachelorThesis.Device, join_through: BachelorThesis.UserDevice, on_delete: :delete_all

    timestamps()
  end

  @doc """
  Builds a changeset based on the `struct` and `params`.
  """
  def changeset(struct, params \\ %{}) do
    struct
    |> cast(params, [:email, :password, :firstName, :lastName, :admin])
    |> validate_required([:email, :password, :firstName, :lastName, :admin])
    |> unique_constraint(:email)
  end

  @doc """
  Function returns User by email and password
  """
  def get_by_email_password(email, password, preload) do
    query = from u in User,
      where: u.email == ^email and u.password == ^password
    # this could break if more than theres more than 1 user with same name.
    user = Repo.one(query)
    case user do
      nil -> nil
      _ -> get_by_id(user.id, preload)
    end
  end

  @doc """
  Function returns SmartDevice by selected id.
  Parameter preload is used to decide if returned user contains preloaded smart devices and devices.
  """
  def get_by_id(id, preload) do
    case preload do
      true -> Repo.get(User, id) |> Repo.preload([:smart_devices, :devices])
      _    -> Repo.get(User, id)
    end

  end
end
