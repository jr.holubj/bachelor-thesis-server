defmodule BachelorThesis.Device do
  use BachelorThesis.Web, :model

  alias BachelorThesis.Device
  alias BachelorThesis.Repo

  schema "devices" do
    field :token, :string
    field :name, :string
    field :description, :string
    field :pairedUp, :boolean, default: false
    field :active, :boolean, default: false
    many_to_many :users, BachelorThesis.User, join_through: BachelorThesis.UserDevice, on_delete: :delete_all
    has_many :pair_devices, BachelorThesis.PairDevice, on_delete: :delete_all
    has_many :values, BachelorThesis.DeviceValue, on_delete: :delete_all
    has_many :actions, BachelorThesis.DeviceAction, on_delete: :delete_all
    belongs_to :interface_type, BachelorThesis.DeviceInterfaceType

    timestamps()
  end

  @doc """
  Builds a changeset based on the `struct` and `params`.
  """
  def changeset(struct, params \\ %{}) do
    struct
    |> cast(params, [:token, :name, :description, :pairedUp, :active, :interface_type_id])
    |> validate_required([:token, :name, :description, :pairedUp, :active])
    |> unique_constraint(:token)
  end

  @doc """
  Function returns device by selected token
  """
  def get_by_token(token) do
    query = from u in Device,
      where: u.token == ^token
    Repo.one(query) |> Repo.preload([:users, :interface_type])
  end

  @doc """
  Function returns device by selected id
  """
  def get_by_id(id) do
    query = from u in Device,
      where: u.id == ^id
    Repo.one(query) |> Repo.preload([:interface_type])
  end

  @doc """
  Function returns map which contains values by interface type
  """
  def setValue(interface_shortcut, value) do
    case interface_shortcut do
      "yn" -> %{"boolValue" => value, "intValue" => nil}
      "num" -> %{"boolValue" => nil, "intValue" => value}
      "st" -> %{"boolValue" => value, "intValue" => nil}
    end
  end
end
