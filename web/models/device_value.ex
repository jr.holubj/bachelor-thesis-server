defmodule BachelorThesis.DeviceValue do
  use BachelorThesis.Web, :model

  schema "device_values" do
    field :intValue, :integer
    field :boolValue, :boolean, default: false
    field :datetime, Ecto.DateTime
    belongs_to :device, BachelorThesis.Device
    belongs_to :device_interface_type, BachelorThesis.DeviceInterfaceType

    timestamps()
  end

  @doc """
  Builds a changeset based on the `struct` and `params`.
  """
  def changeset(struct, params \\ %{}) do
    struct
    |> cast(params, [:intValue, :boolValue, :datetime, :device_id, :device_interface_type_id])
  end
end
