defmodule BachelorThesis.UserDevice do
  use BachelorThesis.Web, :model

  schema "user_devices" do
    belongs_to :user, BachelorThesis.User
    belongs_to :device, BachelorThesis.Device

    timestamps()
  end

  @doc """
  Builds a changeset based on the `struct` and `params`.
  """
  def changeset(struct, params \\ %{}) do
    struct
    |> cast(params, [:user_id, :device_id])
    |> validate_required([])
  end
end
