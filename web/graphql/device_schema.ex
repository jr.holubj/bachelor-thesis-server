defmodule GraphQL.Schema.DeviceSchema do
  alias GraphQL.Schema
  alias GraphQL.Type.{ObjectType, List, NonNull, ID, String, Int, Boolean}
  alias GraphQL.Schema.DeviceSchema

  alias BachelorThesis.Device
  alias BachelorThesis.SmartDevice
  alias BachelorThesis.User
  alias BachelorThesis.ViewFunctions
  alias BachelorThesis.Repo

  def schema do
    %Schema{
      query: %ObjectType{
        name: "Device",
        fields: %{
          device: %{
            type: %List{ofType: %String{}},
            args: %{
              token: %{type: %String{}}
            },
            resolve: {Schema.DeviceSchema, :getDevicesByToken}
          }
        }
      }
    }
  end

  def getDevicesByToken(_source, %{token: token}, _info) do
    smartDevice = SmartDevice.get_by_token(token)
    case smartDevice do
      nil -> %{}
      _   ->
        case smartDevice.pairedUp == true and smartDevice.active == true do
          false -> %{}
          true  ->
            user = User.get_by_id(smartDevice.user_id, true)
            case user do
              nil -> %{}
              _   ->
                for device <- user.devices do
                  case device.active == true and device.pairedUp == true do
                    true -> getDevice(device.id)
                    _    -> %{}
                  end
                end
            end
        end
    end
  end

  def getDevice(id) do

    device = Device.get_by_id(id)
    value = ViewFunctions.getDeviceValueByDevice(id)
    action = ViewFunctions.getDeviceActionByDevice2(id)
    case device do
      nil -> %{}
      _   ->
        %{
          id: device.id,
          name: device.name,
          value: value,
          action: action,
          token: device.token,
          interface_type: %{
            id: device.interface_type.id,
            shortcut: device.interface_type.shortcut
          }
        }
    end
  end
end
