defmodule BachelorThesis.Router do
  use BachelorThesis.Web, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  pipeline :authentication do
    plug BachelorThesis.Plug.Authenticate
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/", BachelorThesis do
    pipe_through :browser # Use the default browser stack

    get "/", PageController, :index

    resources "/users", UserController, only: [:create, :new]

    # login session
    post "/login", SessionController, :create
    delete "/logout", SessionController, :delete

    # pair up device by token
    get "/pairUpDevice/:token", DeviceController, :pairUpDeviceByToken
    get "/pairUpSmartDevice/:token", SmartDeviceController, :pairUpSmartDeviceByToken

    # control device by smart device
    get "/setDeviceAction/:device_token/:smart_device_token/:interface_type_shortcut/:value", DeviceActionController, :setAction
    # device info
    get "/setDeviceValue/:device_token/:interface_type_shortcut/:value", DeviceValueController, :setValue
    # confirm device action
    get "/confirmAction/:device_token/:action_id", DeviceActionController, :confirmAction
    # device info
    get "/getDeviceValue/:device_token", DeviceValueController, :getValue
    get "/getDeviceAction/:device_token", DeviceActionController, :getAction
    end

  scope "/", BachelorThesis do
    pipe_through [:browser, :authentication]

    resources "/users", UserController, except: [:create, :new, :index] do
      # smart_devices
      resources "/smart_devices", SmartDeviceController
      post "/smart_device/:smart_device_id/pair", SmartDeviceController, :pairDevice
      delete "/smart_device/:smart_device_id/delete/:id", SmartDeviceController, :deletePairDevice
      post "/smart_device/:smart_device_id/unpairSmartDevice/", SmartDeviceController, :unpairSmartDevice
      post "/smart_device/:smart_device_id/activateSmartDevice/:action", SmartDeviceController, :activateSmartDevice

      # devices
      resources "/devices", DeviceController
      post "/device/:device_id/pair", DeviceController, :pairDevice
      delete "/device/:device_id/delete/:id", DeviceController, :deletePairDevice
      post "/device/:device_id/unpairDevice/", DeviceController, :unpairDevice
      post "/device/:device_id/activateDevice/:action", DeviceController, :activateDevice
    end
  end

  # Other scopes may use custom stacks.
  # scope "/api", BachelorThesis do
  #   pipe_through :api
  # end

  scope "/graphql" do
    pipe_through :api

    # Welcome API
    get "/api", BachelorThesis.PageController, :api
    post "/api", BachelorThesis.PageController, :api_post

    get "/device", GraphQL.Plug, schema: {GraphQL.Schema.DeviceSchema, :schema}
    post "/device", GraphQL.Plug, schema: {GraphQL.Schema.DeviceSchema, :schema}
  end
end
