defmodule BachelorThesis.DeviceActionController do
  use BachelorThesis.Web, :controller

  alias BachelorThesis.DeviceAction
  alias BachelorThesis.Device
  alias BachelorThesis.SmartDevice
  alias BachelorThesis.DeviceInterfaceType
  alias BachelorThesis.Generator
  alias BachelorThesis.ViewFunctions
  alias BachelorThesis.Repo

  import Ecto.Changeset

  @doc """
  Function returns last X DeviceAction for device where X is parameter
  """
  def latest(count, device_id) do
    Repo.all(
     from a in DeviceAction,
     where: a.device_id == ^device_id,
     limit: ^count,
     order_by: [desc: a.inserted_at]
    ) |> Repo.preload([:smart_device])
  end

  @doc """
  This function handles request usually by smart watches to set new action for device
  """
  def setAction(conn, %{"device_token" => device_token, "smart_device_token" => smart_device_token, "interface_type_shortcut" => interface_type_shortcut, "value" =>
  value} = params) do
    device = Device.get_by_token(device_token)
    smart_device = SmartDevice.get_by_token(smart_device_token)
    interface_type = DeviceInterfaceType.get_by_shortcut(interface_type_shortcut)

    case  device == nil ||
          smart_device == nil ||
          smart_device.active == false ||
          smart_device.pairedUp == false ||
          interface_type == nil ||
          isValueInList(smart_device.user_id, device.users) == false ||
          device.active == false ||
          device.pairedUp == false ||
          device.interface_type.shortcut != interface_type.shortcut do
      true  ->
        json conn, %{"success" => false}
      false ->
        values = Device.setValue(interface_type.shortcut, value)
        map = %{"device_id" => device.id, "smart_device_id" => smart_device.id, "device_interface_type_id" => interface_type.id, "confirmed" => false,
          "datetime" => Generator.createDateTime(0)}
        map = Map.merge(map, values)
        changeset = DeviceAction.changeset(%DeviceAction{}, map)
        case Repo.insert(changeset) do
          {:ok, _} ->
            json conn, %{"success" => true}
          {:error, changeset} ->
            json conn, %{"success" => changeset}
        end
    end
  end

  @doc """
  Function returns DeviceAction for device deifned by it's token
  """
  def getAction(conn, %{"device_token" => device_token}) do
    device = Device.get_by_token(device_token)

    case device == nil do
      true  ->
        json conn, %{}
      false ->
        action = ViewFunctions.getDeviceActionByDevice(device.id)
        case action do
          nil -> json conn, %{}
          _   ->
            json conn, %{
              "value" => action.value,
              "confirmed" => action.confirmed,
              "action_id" => action.id,
              "interface_type" => %{
                 "id" => device.interface_type.id,
                 "shortcut" => device.interface_type.shortcut
              }
            }
        end
    end
  end

  defp isValueInList(user_id, users) do
    Enum.any?(users, fn(item) -> item.id == user_id end)
  end

  @doc """
  Function returns DeviceAction for device
  """
  def get_action(device_id) do
    query = from u in DeviceAction,
      where: u.device_id == ^device_id
    device = DeviceAction |> where([i], i.device_id == ^device_id) |> last |> Repo.one
    device
  end

  def confirmAction(conn, %{"device_token" => device_token, "action_id" => action_id}) do
    device = Device.get_by_token(device_token)
    action = DeviceAction.get_by_id(action_id)

    if device != nil && action != nil && device.id == action.device_id do
      changeset = DeviceAction.changeset(action, %{"confirmed" => true})
      Repo.update changeset
      json conn, %{"success" => true}
    end
    json conn, %{"success" => false}
  end
end
