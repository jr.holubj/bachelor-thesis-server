defmodule BachelorThesis.SessionController do
  use BachelorThesis.Web, :controller

  alias BachelorThesis.Session

  def create(conn, %{"login" => login}) do
    conn = Session.login(conn, login)
    case Session.logged_in?(conn) do
      true ->
        conn
          |> put_flash(:info, "You are logged in")
          |> redirect(to: "/")
      _ ->
        conn
          |> put_flash(:error, "Invalid email or password")
          |> redirect(to: "/")
    end
  end

  def delete(conn, _params) do
    conn
      |> Session.delete
      |> put_flash(:info, "You are logged out")
      |> redirect(to: "/")
  end
end
