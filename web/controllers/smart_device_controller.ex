defmodule BachelorThesis.SmartDeviceController do
  use BachelorThesis.Web, :controller

  alias BachelorThesis.SmartDevice
  alias BachelorThesis.PairSmartDevice
  alias BachelorThesis.User
  alias BachelorThesis.Generator

  import Ecto.Changeset, only: [put_change: 3]
  import Ecto.Query
  import Ecto.Date

  def index(conn, _params) do
    smart_devices = Repo.all(SmartDevice)
    render(conn, "index.html", smart_devices: smart_devices)
  end

  def new(conn, %{"user_id" => user_id}) do
    user = User.get_by_id(user_id, false)
    changeset = SmartDevice.changeset(%SmartDevice{})
    render(conn, "new.html", [changeset: changeset, user: user])
  end

  def create(conn, %{"smart_device" => smart_device_params, "user_id" => user_id}) do
    token = Generator.generateString(50)
    changeset = SmartDevice.changeset(%SmartDevice{}, Map.merge(smart_device_params, %{"user_id" => user_id, "token" => token}))

    case Repo.insert(changeset) do
      {:ok, _smart_device} ->
        conn
        |> put_flash(:info, "Smart device created successfully.")
        |> redirect(to: user_path(conn, :show, user_id))
      {:error, changeset} ->
        user = User.get_by_id(user_id, false)
        render(conn, "new.html", changeset: changeset, user: user)
    end
  end

  def show(conn, %{"id" => id, "user_id" => user_id}) do
    user = User.get_by_id(user_id, false)
    smart_device = Repo.get(SmartDevice, id) |> Repo.preload([:pair_smart_devices])
    conn |> validation(smart_device, user)
    render(conn, "show.html", smart_device: smart_device, user: user)
  end

  def edit(conn, %{"id" => id, "user_id" => user_id}) do
    user = User.get_by_id(user_id, false)
    smart_device = Repo.get!(SmartDevice, id)
    changeset = SmartDevice.changeset(smart_device)
    render(conn, "edit.html", smart_device: smart_device, changeset: changeset, user: user)
  end

  def update(conn, %{"id" => id, "smart_device" => smart_device_params, "user_id" => user_id}) do
    update(conn, id, smart_device_params, user_id)
  end

  def update(conn, id, smart_device_params, user_id) do
    smart_device = Repo.get!(SmartDevice, id)
    changeset = SmartDevice.changeset(smart_device, smart_device_params)
    case Repo.update(changeset) do
      {:ok, smart_device} ->
        conn
        |> put_flash(:info, "Smart device updated successfully.")
        |> redirect(to: user_path(conn, :show, user_id))
      {:error, changeset} ->
        user = User.get_by_id(user_id, false)
        render(conn, "edit.html", smart_device: smart_device, changeset: changeset, user: user)
    end
  end

  def delete(conn, %{"id" => id, "user_id" => user_id}) do
    smart_device = Repo.get!(SmartDevice, id)

    # Here we use delete! (with a bang) because we expect
    # it to always work (and if it does not, it will raise).
    Repo.delete!(smart_device)

    conn
    |> put_flash(:info, "Smart device deleted successfully.")
    |> redirect(to: user_path(conn, :show, user_id))
  end


  defp validation(conn, device, user) do
    if device == nil || user == nil do
      redirectErrorPage(conn)
    end

    user_id = user.id
    case device.user_id do
      ^user_id -> true
      _ -> redirectErrorPage(conn)
    end
  end

  defp redirectErrorPage(conn) do
    conn
     |> put_flash(:error, "Upps, something went wrong!")
     |> redirect(to: page_path(conn, :index))
  end

  @doc """
  Function insert pair up request to database
  """
  def pairDevice(conn, %{"smart_device_id" => smart_device_id, "user_id" => user_id}) do
    token = Generator.generateString(6)
    map = %{"smart_device_id" => smart_device_id, "token" => token, "deadline" => Generator.createDateTime(5), "successful" => false}
    changeset = PairSmartDevice.changeset(%PairSmartDevice{}, map)

    case Repo.insert(changeset) do
      {:ok, _} ->
        conn
        |> put_flash(:info, "Request was successful.")
        |> redirect(to: user_smart_device_path(conn, :show, user_id, smart_device_id))
      {:error, changeset} ->
        conn
        |> put_flash(:error, "Uppps...Something went wrong.")
        |> redirect(to: user_smart_device_path(conn, :show, user_id, smart_device_id))
    end
  end

  @doc """
  Function delete pair up request from database
  """
  def deletePairDevice(conn, %{"id" => id, "user_id" => user_id, "smart_device_id" => smart_device_id} = params) do
    IO.inspect(params)
    pair_smart_device = Repo.get!(PairSmartDevice, id)
    Repo.delete!(pair_smart_device)

    conn
    |> put_flash(:info, "Item deleted successfully.")
    |> redirect(to: user_smart_device_path(conn, :show, user_id, smart_device_id))
  end

  @doc """
  Function activates device
  """
  def activateSmartDevice(conn, %{"user_id" => user_id, "smart_device_id" => smart_device_id, "action" => action}) do
    update(conn, smart_device_id, %{active: action}, user_id)
  end

  @doc """
  Function deactivates device
  """
  def unpairSmartDevice(conn, %{"user_id" => user_id, "smart_device_id" => smart_device_id}) do
    update(conn, smart_device_id, %{pairedUp: false, active: false}, user_id)
  end

  @doc """
  Function returns if there is currently pair up request
  """
  def existsPairUpRequest(smart_device_id) do
    now = Ecto.DateTime.utc
    query = from p in "pair_smart_device",
            where: p.smart_device_id == ^smart_device_id and p.deadline > ^now and p.successful == ^false,
            select: {p.id}
    result = Repo.all(query)

    case length result do
      0 -> false
      _   -> true
    end
  end


  @doc """
  Function pair up device using token. Function return token in JSON format
  """
  def pairUpSmartDeviceByToken(conn, %{"token" => token}) do
    pairDevice = PairSmartDevice.get_by_token(token)
    case pairDevice do
      nil ->
        json conn, %{"token" => ""}
      _   ->
        case pairDevice.token == token do
          true  ->
            changeset = PairSmartDevice.changeset(pairDevice, %{successful: true})
            case Repo.update(changeset) do
              {:ok, _params} ->
                # update smart_device -> set pairedUp: true, active: true
                updateSmartDevice(pairDevice.smart_device.id, %{pairedUp: true, active: true})
                json conn, %{"token" => pairDevice.smart_device.token}
            end
        end
    end
    json conn, %{"token" => ""}
  end


  def updateSmartDevice(id, device_params) do
    device = Repo.get!(SmartDevice, id)
    changeset = SmartDevice.changeset(device, device_params)

    case Repo.update(changeset) do
      {:ok, device} ->
        true
      {:error, changeset} ->
        false
    end
  end
end
