defmodule BachelorThesis.DeviceController do
  use BachelorThesis.Web, :controller

  alias BachelorThesis.Device
  alias BachelorThesis.DeviceActionController
  alias BachelorThesis.DeviceValueController
  alias BachelorThesis.PairDevice
  alias BachelorThesis.DeviceInterfaceType
  alias BachelorThesis.User
  alias BachelorThesis.UserDevice
  alias BachelorThesis.Generator

  import Ecto.Changeset, only: [put_change: 3]

  def index(conn, _params) do
    devices = Repo.all(Device)
    render(conn, "index.html", devices: devices)
  end

  def new(conn, %{"user_id" => user_id}) do
    user = User.get_by_id(user_id, false)
    interfaces = Repo.all(DeviceInterfaceType)
    changeset = Device.changeset(%Device{})
    render(conn, "new.html", changeset: changeset, user: user, interfaces: interfaces)
  end

  def create(conn, %{"device" => device_params, "user_id" => user_id}) do
    token = Generator.generateString(50)
    changeset = Device.changeset(%Device{}, Map.merge(device_params, %{"token" => token}))

    case Repo.insert(changeset) do
      {:ok, device} ->
        changesetUserDevice = UserDevice.changeset(%UserDevice{}, %{"user_id" => user_id, "device_id" => device.id})
        Repo.insert(changesetUserDevice)
        conn
          |> put_flash(:info, "Device created successfully.")
          |> redirect(to: user_path(conn, :show, user_id))
      {:error, changeset} ->
        user = User.get_by_id(user_id, false)
        interfaces = Repo.all(DeviceInterfaceType)
        render(conn, "new.html", changeset: changeset, user: user, interfaces: interfaces)
    end
  end

  def show(conn, %{"id" => id, "user_id" => user_id}) do
    user = User.get_by_id(user_id, false)
    device = Repo.get!(Device, id) |> Repo.preload([:pair_devices, :interface_type])
    actions = DeviceActionController.latest(15, device.id)
    values = DeviceValueController.latest(15, device.id)
    render(conn, "show.html", device: device, user: user, actions: actions, values: values)
  end

  def edit(conn, %{"id" => id, "user_id" => user_id}) do
    user = User.get_by_id(user_id, false)
    device = Repo.get!(Device, id)
    changeset = Device.changeset(device)
    interfaces = Repo.all(DeviceInterfaceType)
    render(conn, "edit.html", device: device, changeset: changeset, user: user, interfaces: interfaces)
  end


  def update(conn, %{"id" => id, "device" => device_params, "user_id" => user_id}) do
    update(conn, id, device_params, user_id)
  end

  def update(conn, id, device_params, user_id) do
    device = Repo.get!(Device, id)
    changeset = Device.changeset(device, device_params)

    case Repo.update(changeset) do
      {:ok, _device} ->
        conn
        |> put_flash(:info, "Device updated successfully.")
        |> redirect(to: user_path(conn, :show, user_id))
      {:error, changeset} ->
        render(conn, "edit.html", device: device, changeset: changeset)
    end
  end


  def delete(conn, %{"id" => id, "user_id" => user_id}) do
    device = Repo.get!(Device, id)

    # Here we use delete! (with a bang) because we expect
    # it to always work (and if it does not, it will raise).
    Repo.delete!(device)

    conn
    |> put_flash(:info, "Device deleted successfully.")
    |> redirect(to: user_path(conn, :show, user_id))
  end


  @doc """
  Function insert pair up request to database
  """
  def pairDevice(conn, %{"device_id" => device_id, "user_id" => user_id}) do
    token = Generator.generateString(6)
    map = %{"device_id" => device_id, "token" => token, "deadline" => Generator.createDateTime(5), "successful" => false}
    changeset = PairDevice.changeset(%PairDevice{}, map)

    case Repo.insert(changeset) do
      {:ok, _} ->
        conn
        |> put_flash(:info, "Request was successful.")
        |> redirect(to: user_device_path(conn, :show, user_id, device_id))
      {:error, _changeset} ->
        conn
        |> put_flash(:error, "Uppps...Something went wrong.")
        |> redirect(to: user_device_path(conn, :show, user_id, device_id))
    end
  end


  @doc """
  Function delete pair up request from database
  """
  def deletePairDevice(conn, %{"id" => id, "user_id" => user_id, "device_id" => device_id}) do
    pair_device = Repo.get!(PairDevice, id)
    Repo.delete!(pair_device)

    conn
    |> put_flash(:info, "Item deleted successfully.")
    |> redirect(to: user_device_path(conn, :show, user_id, device_id))
  end

  @doc """
  Function activates device
  """
  def activateDevice(conn, %{"user_id" => user_id, "device_id" => device_id, "action" => action}) do
    update(conn, device_id, %{active: action}, user_id)
  end

  @doc """
  Function deactivates device
  """
  def unpairDevice(conn, %{"user_id" => user_id, "device_id" => device_id}) do
    update(conn, device_id, %{pairedUp: false, active: false}, user_id)
  end

  @doc """
  Function returns if there is currently pair up request
  """
  def existsPairUpRequest(device_id) do
    now = Ecto.DateTime.utc
    query = from p in "pair_devices",
            where: p.device_id == ^device_id and p.deadline > ^now and p.successful == ^false,
            select: {p.id}
    result = Repo.all(query)

    case length result do
      0 -> false
      _   -> true
    end
  end

  @doc """
  Function pair up device using token. Function return token in JSON format
  """
  def pairUpDeviceByToken(conn, %{"token" => token}) do
    pairDevice = PairDevice.get_by_token(token)
    case pairDevice do
      nil ->
        json conn, %{"token" => ""}
      _   ->
        case pairDevice.token == token do
          true  ->
            changeset = PairDevice.changeset(pairDevice, %{successful: true})
            case Repo.update(changeset) do
              {:ok, _params} ->
                # update smart_device -> set pairedUp: true, active: true
                updateDevice(pairDevice.device.id, %{pairedUp: true, active: true})
                json conn, %{"token" => pairDevice.device.token}
            end
        end
    end
    json conn, %{"token" => ""}
  end


  def updateDevice(id, device_params) do
    device = Repo.get!(Device, id)
    changeset = Device.changeset(device, device_params)

    case Repo.update(changeset) do
      {:ok, _device} ->
        true
      {:error, _changeset} ->
        false
    end
  end
end
