defmodule BachelorThesis.DeviceValueController do
  use BachelorThesis.Web, :controller

  alias BachelorThesis.DeviceValue
  alias BachelorThesis.Device
  alias BachelorThesis.DeviceInterfaceType
  alias BachelorThesis.Generator
  alias BachelorThesis.Repo
  alias BachelorThesis.ViewFunctions

  @doc """
  Function returns last X DeviceValue for device where X is parameter
  """
  def latest(count, device_id) do
    Repo.all(
     from a in DeviceValue,
     where: a.device_id == ^device_id,
     limit: ^count,
     order_by: [desc: a.inserted_at]
    )
  end

  @doc """
  This function handles request by device to set new value
  """
  def setValue(conn, %{"device_token" => device_token, "interface_type_shortcut" => interface_type_shortcut, "value" => value} = params) do
    device = Device.get_by_token(device_token)
    interface_type = DeviceInterfaceType.get_by_shortcut(interface_type_shortcut)

    case device == nil ||
         interface_type == nil ||
         device.interface_type.shortcut != interface_type.shortcut do
      true  ->
        json conn, %{"success" => false}
      false ->
        values = Device.setValue(interface_type.shortcut, value)
        map = %{"device_id" => device.id, "device_interface_type_id" => interface_type.id, "datetime" => Generator.createDateTime(0)}
        map = Map.merge(map, values)
        changeset = DeviceValue.changeset(%DeviceValue{}, map)
        case Repo.insert(changeset) do
          {:ok, _} ->
            json conn, %{"success" => true}
          {:error, changeset} ->
            json conn, %{"success" => changeset}
        end
    end
  end

  @doc """
  Function returns DeviceValue for device deifned by it's token
  """
  def getValue(conn, %{"device_token" => device_token}) do
    device = Device.get_by_token(device_token)

    case device == nil do
      true  ->
        json conn, %{}
      false ->
          value = ViewFunctions.getDeviceValueByDevice(device.id)
          json conn, %{
            "value" => value,
            "interface_type" => %{
               "id" => device.interface_type.id,
               "shortcut" => device.interface_type.shortcut
             }}
    end
  end


  @doc """
  Function returns DeviceValue for device
  """
  def get_value(device_id) do
    query = from u in DeviceValue,
      where: u.device_id == ^device_id
    DeviceValue |> where([i], i.device_id == ^device_id) |> last |> Repo.one
  end

end
