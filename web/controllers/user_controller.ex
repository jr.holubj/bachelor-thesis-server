defmodule BachelorThesis.UserController do
  use BachelorThesis.Web, :controller

  alias BachelorThesis.User
  alias BachelorThesis.Hash
  alias BachelorThesis.Session
  import Ecto.Changeset, only: [put_change: 3]

  def index(conn, _params) do
    users = Repo.all(User)
    render(conn, "index.html", users: users)
  end

  def new(conn, _params) do
    changeset = User.changeset(%User{})
    render(conn, "new.html", changeset: changeset)
  end

  def create(conn, %{"user" => user_params}) do
    changeset = User.changeset(%User{}, user_params) |> put_change(:password, Hash.hashString(user_params["password"]))

    case Repo.insert(changeset) do
      {:ok, _user} ->
        conn
          |> put_flash(:info, "User created successfully.")
          |> Session.login(user_params)
          |> redirect(to: page_path(conn, :index))
      {:error, err_changeset} ->
        render(conn, "new.html", changeset: err_changeset)
    end
  end

  def show(conn, %{"id" => id}) do
    user = User.get_by_id(id, true)
    conn |> validation(user)
    render(conn, "show.html", user: user)
  end

  def edit(conn, %{"id" => id}) do
    user = Repo.get!(User, id)
    changeset = User.changeset(user)
    render(conn, "edit.html", user: user, changeset: changeset)
  end

  def update(conn, %{"id" => id, "user" => user_params}) do
    user = Repo.get!(User, id)
    changeset = User.changeset(user, user_params)

    case Repo.update(changeset) do
      {:ok, user} ->
        conn
          |> put_flash(:info, "User updated successfully.")
          |> redirect(to: user_path(conn, :show, user))
      {:error, changeset} ->
        render(conn, "edit.html", user: user, changeset: changeset)
    end
  end

  def delete(conn, %{"id" => id}) do
    user = Repo.get!(User, id)

    # Here we use delete! (with a bang) because we expect
    # it to always work (and if it does not, it will raise).
    Repo.delete!(user)

    conn
      |> put_flash(:info, "User deleted successfully.")
      |> redirect(to: user_path(conn, :index))
  end

  defp validation(conn, user) do
      current_user = Session.current_user(conn)
      if current_user == nil || user == nil do
        redirectErrorPage(conn)
      end

      current_user_id = current_user.id
      case user.id do
        ^current_user_id -> true
        _ -> redirectErrorPage(conn)
      end
    end

    defp redirectErrorPage(conn) do
      conn
       |> put_flash(:error, "Upps, something went wrong!")
       |> redirect(to: page_path(conn, :index))
    end
end
