defmodule BachelorThesis.PageController do
  use BachelorThesis.Web, :controller

  alias BachelorThesis.Session

  def index(conn, _params) do
    user = Session.current_user(conn)
    case user do
      nil -> render conn, "index.html"
      _ -> conn |> redirect(to: user_path(conn, :show, user))
    end
  end

  def api(conn, _params) do
    json conn, %{"message" => "Welcome to server API", "method" => "GET"}
  end
  def api_post(conn, _params) do
    json conn, %{"message" => "Welcome to server API", "method" => "POST"}
  end
end
