defmodule BachelorThesis.Generator do
  @doc """
  Function generates string
  """
  def generateString(lenght) do
    :crypto.strong_rand_bytes(lenght) |> Base.url_encode64 |> binary_part(0, lenght)
  end

  @doc """
  Function generates current dateTime plus X minutes where X is parameter
  """
  def createDateTime(plusMinutes) do
    :erlang.universaltime
      |> :calendar.datetime_to_gregorian_seconds
      |> Kernel.+(plusMinutes * 60)
      |> :calendar.gregorian_seconds_to_datetime
      |> Ecto.DateTime.cast!
  end
end
