defmodule BachelorThesis.ViewFunctions do
  import Ecto.DateTime


  @doc """
  Function returns dateTime in right format
  """
  def date_format(date) do
    "#{date.day}.#{date.month}.#{date.year} #{date.hour}:#{date.min}:#{date.sec}"
  end

  @doc """
  Function prints boolean value
    Returns
      Yes if boolean == true
      No if boolean == false
      Undefined else
  """
  def yesNo(boolean) do
    case boolean do
      true  -> "Yes"
      false -> "No"
      _     -> "Undefined"
    end
  end

  @doc """
    Compare datetime to now
    Returns
      0 if datetime == now
      1 if datetime < now
      -1 if datetime > now
  """
  def compareDatetimeToNow(datetime) do
    now = Ecto.DateTime.utc
    case compare(datetime,now) do
      :gt -> -1
      :lt -> 1
      :eq -> 0
    end
  end

  defp compareDatetimes

  @doc """
    return:
      true  -> request exists
      false -> request doesn't exist
  """
  def existsPairUpRequestSmartDevice(smart_device_id) do
    BachelorThesis.SmartDeviceController.existsPairUpRequest(smart_device_id)
  end

  @doc """
    return:
      true  -> request exists
      false -> request doesn't exist
  """
  def existsPairUpRequestDevice(device_id) do
    BachelorThesis.DeviceController.existsPairUpRequest(device_id)
  end

  @doc """
    Returns right value from set od values
  """
  def getDeviceValue(values) do
    case values do
      nil -> ""
      _   ->
        case values.intValue != nil do
          true  -> values.intValue
          false -> values.boolValue
        end
    end
  end

  @doc """
    Return value of device by device id
  """
  def getDeviceValueByDevice(device_id) do
     BachelorThesis.DeviceValueController.get_value(device_id) |> getDeviceValue
  end

  @doc """
    Return value of device by device id
  """
  def getDeviceActionByDevice2(device_id) do
    action = BachelorThesis.DeviceActionController.get_action(device_id) |> getDeviceValue
  end

  @doc """
    Return value of device by device id
  """
  def getDeviceActionByDevice(device_id) do
    action = BachelorThesis.DeviceActionController.get_action(device_id)
    case action do
      nil -> nil
      _   ->
        value = action |> getDeviceValue
        Map.merge(action, %{:value => value})
    end
  end
end
