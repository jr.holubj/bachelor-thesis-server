defmodule BachelorThesis.Session do
  import Plug.Conn
  alias Session
  alias BachelorThesis.User
  alias BachelorThesis.Hash


  @doc """
  Function puts user to session
  """
  def login(conn, %{"email" => email, "password" => password}) do
    user = User.get_by_email_password(email, Hash.hashString(password), false)
    case user do
      nil ->
          conn
      _   ->
          conn
            |> put_session(:current_user, user)
    end
  end

  @doc """
  Function returns if user is logged in
  """
  def logged_in?(conn) do
    case current_user(conn) do
      nil -> false
      _   -> true
    end
  end

  @doc """
  Function returns currently logged user
  """
  def current_user(conn) do
    conn
      |> get_session(:current_user)
  end


  @doc """
  Function deletes user from session
  """
  def delete(conn) do
    conn
      |> delete_session(:current_user)
  end
end
