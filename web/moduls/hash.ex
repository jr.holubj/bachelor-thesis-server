defmodule BachelorThesis.Hash do


  @doc """
  Function returns hashed string
  """
  def hashString(string) do
    "hashed_string_" <> string <> "_hashed_string"
  end
end
